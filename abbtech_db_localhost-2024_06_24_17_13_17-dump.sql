--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11
-- Dumped by pg_dump version 15.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: calculation_result; Type: TABLE; Schema: public; Owner: abbtech_user
--

CREATE TABLE public.calculation_result (
    id integer NOT NULL,
    variable_a integer NOT NULL,
    variable_b integer NOT NULL,
    calc_result integer NOT NULL,
    calc_method character varying(255) NOT NULL
);


ALTER TABLE public.calculation_result OWNER TO abbtech_user;

--
-- Name: calculation_result_id_seq; Type: SEQUENCE; Schema: public; Owner: abbtech_user
--

ALTER TABLE public.calculation_result ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.calculation_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: calculation_result; Type: TABLE DATA; Schema: public; Owner: abbtech_user
--

COPY public.calculation_result (id, variable_a, variable_b, calc_result, calc_method) FROM stdin;
\.


--
-- Name: calculation_result_id_seq; Type: SEQUENCE SET; Schema: public; Owner: abbtech_user
--

SELECT pg_catalog.setval('public.calculation_result_id_seq', 6, true);


--
-- Name: calculation_result calculation_result_pk; Type: CONSTRAINT; Schema: public; Owner: abbtech_user
--

ALTER TABLE ONLY public.calculation_result
    ADD CONSTRAINT calculation_result_pk PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

