package org.abbtech;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CalculationRepository {
    public void saveCalculationResult(int a, int b, int result, String calculationMethod) {
        try {
            String sqlInsert = """
                insert into calculation_result(variable_a,variable_b,calc_result,calc_method)
                 values (?,?,?,?)
                """;
            Connection connection = JDBCConnection.getConnection();
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, result);
            preparedStatement.setString(4, calculationMethod);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
