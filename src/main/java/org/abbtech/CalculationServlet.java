package org.abbtech;

import com.google.gson.Gson;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/create"})
public class CalculationServlet extends HttpServlet {
    private Gson gson = new Gson();
    private ApplicationService applicationService;
    private CalculationRepository calculationRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
        calculationRepository = new CalculationRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        ResponseDTO responseObject = new ResponseDTO();
        String jsonResponse = null;
        try {
            String calculationMethod = req.getHeader("x-calculation-method");
            if(calculationMethod == null){
                resp.setStatus(400);
                writer.print(ResponseDTO.createResponse("error", "x-calculation-method not found in header request" ,null));
                writer.flush();
                return;
            }
            calculationMethod = calculationMethod.toLowerCase();
            String parA = req.getParameter("a");
            String parB = req.getParameter("b");
            if(parA == null || parB == null){
                resp.setStatus(400);
                writer.print(ResponseDTO.createResponse("error", "query parameter a or b not found" ,null));
                writer.flush();
                return;
            }
            int a = Integer.parseInt(parA);
            int b = Integer.parseInt(parB);
            int result = 0;

            switch (calculationMethod) {
                case "multiply":
                    result = applicationService.multiply(a, b);
                    break;
                case "add":
                    result = applicationService.add(a, b);
                    break;
                case "divide":
                    result = applicationService.divide(a, b);
                    break;
                case "subtract":
                    result = applicationService.subtract(a, b);
                    break;
                default:
                    resp.setStatus(400);
                    writer.print(ResponseDTO.createResponse("error", "not a valid method" ,null));
                    writer.flush();
                    return;
            };
            calculationRepository.saveCalculationResult(a, b, result, calculationMethod);
            resp.setStatus(HttpServletResponse.SC_CREATED);
            writer.print(ResponseDTO.createResponse("success", "Calculation Method Created Successfully" ,result));
            writer.flush();
        } catch (RuntimeException exception) {
            resp.setStatus(400);
            writer.print(ResponseDTO.createResponse("error", exception.getMessage(), null));
            writer.flush();
        }

    }

}
