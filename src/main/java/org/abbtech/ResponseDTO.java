package org.abbtech;

import com.google.gson.Gson;

public class ResponseDTO {
    private String status;
    private String message;
    private Integer result;

    public ResponseDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getResult() {
        return result;
    }

    public ResponseDTO(String status, String message, Integer result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
    public static String createResponse(String status, String message, Integer result){
        Gson gson = new Gson();
        ResponseDTO responseDTO = new ResponseDTO(status,message,result);
        return gson.toJson(responseDTO);
    }
}
