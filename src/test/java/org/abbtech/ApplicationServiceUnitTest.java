package org.abbtech;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(MockitoExtension.class)
public class ApplicationServiceUnitTest {
    @Mock
    CalculatorServiceImpl calculatorService;

    @InjectMocks
    ApplicationService applicationService;

    @Test
    void testMultiply(){
        // * Normal
        when(calculatorService.multiply(1,2)).thenReturn(2);
        int actual = applicationService.multiply(1,2);
        assertEquals(2,actual);
        verify(calculatorService,times(1)).multiply(1,2);

        // * Error
        Exception exception = assertThrows(ArithmeticException.class,()->{
            applicationService.multiply(5,7);
        });
        assertInstanceOf(ArithmeticException.class, exception);
    }
    @Test
    void testSubtract(){
        // * Normal
        when(calculatorService.subtract(2,2)).thenReturn(0);
        int actual = applicationService.subtract(2,2);
        assertEquals(0,actual);
        verify(calculatorService,times(1)).subtract(2,2);

        when(calculatorService.subtract(4,2)).thenReturn(2);
        // * Error
        Exception exception = assertThrows(ArithmeticException.class,()->{
            applicationService.subtract(4,2);
        });
        assertInstanceOf(ArithmeticException.class, exception);
    }
    @Test
    void testAdd(){
        // * Normal
        when(calculatorService.add(2,2)).thenReturn(4);
        int actual = applicationService.add(2,2);
        assertEquals(4,actual);
        verify(calculatorService,times(1)).add(2,2);

        // * Error
        Exception exception = assertThrows(ArithmeticException.class,()->{
            applicationService.add(-1,-3);
        });
        assertInstanceOf(ArithmeticException.class, exception);

    }
    @Test
    void testDivide(){
        // * Normal
        when(calculatorService.divide(2,2)).thenReturn(1);
        int actual = applicationService.divide(2,2);
        assertEquals(1,actual);
        verify(calculatorService,times(1)).divide(2,2);


        // * Error
        when(calculatorService.divide(6,0)).thenThrow(ArithmeticException.class);
        Exception exceptionDivideZero = assertThrows(ArithmeticException.class,()->{
            applicationService.divide(6,0);
        });
        assertInstanceOf(ArithmeticException.class, exceptionDivideZero);
        verify(calculatorService,times(1)).divide(6,0);


        // * Error
        Exception exception = assertThrows(ArithmeticException.class,()->{
            applicationService.divide(5,3);
        });
        assertInstanceOf(ArithmeticException.class, exception);
    }
}
